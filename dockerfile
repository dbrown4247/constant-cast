FROM golang:1.14-alpine AS builder
ENV GOMODULE111=ON
WORKDIR $GOPATH/src/constant-cast/constant-cast
RUN echo $GOPATH
RUN apk add --update --no-cache ca-certificates bash
COPY go.mod .
COPY go.sum .
RUN go mod download

COPY . .
RUN go get -d -v
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /go/bin/goapp
FROM alpine:latest AS
RUN apk upgrade --no-cache \
  && apk add --no-cache \
    musl \
    build-base \
    python3 \
    python3-dev \
    postgresql-dev \
    bash \
    git \
  && pip3 install --no-cache-dir --upgrade pip \
  && rm -rf /var/cache/* \
  && rm -rf /root/.cache/*

RUN cd /usr/bin \
  # && ln -sf easy_install-3.5 easy_install \
  && ln -sf python3 python \
  && ln -sf pip3 pip

RUN pip3 install catt
RUN apk add --update --no-cache ca-certificates
COPY --from=builder /go/bin/goapp /go/bin/goapp
WORKDIR $GOPATH/src/constant-cast/constant-cast
COPY . .
ENTRYPOINT ["/go/bin/goapp"]