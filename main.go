package main

import (
	"constant-cast/models"
	"constant-cast/services"
	"encoding/json"
	"log"
	"os"
	"time"
)

var config models.CastConfig

func init() {
	log.SetFlags(log.Lshortfile)
	file, err := os.Open(`assets/config.json`)
	if err != nil {
		log.Println(err)
		panic(err)
	}
	err = json.NewDecoder(file).Decode(&config)
	if err != nil {
		log.Println(err)
		panic(err)
	}
}

func main() {
	for {
		for k, v := range config.Devices {
			idle, err := services.CheckIdle(k)
			if err != nil {
				log.Println(err)
				continue
			}
			if idle {
				val, err := v.Invoke(k)
				if err != nil {
					log.Println(err)
					continue
				}
				if !val {
					log.Printf("failed to cast to device: %s", k)
				}
			}
		}
		time.Sleep(config.PollingRate)
	}
}
