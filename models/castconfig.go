package models

import (
	"constant-cast/services"
	"encoding/json"
	"errors"
	"log"
	"time"
)

type CastConfig struct {
	PollingRate time.Duration
	Devices     ActionAlias
}

type CastSite struct {
	Type string
	Site string
}

func (e *CastSite) Invoke(device string) (bool, error) {
	if len(e.Site) == 0 {
		return false, errors.New("site must not be blank")
	}
	log.Printf("Recasting site: %s <- %s", e.Site, device)
	return services.CastSite(device, e.Site)
}

type ActionAlias map[string]Action

type Action interface {
	Invoke(device string) (bool, error)
}

var actionMap = map[string]func() Action{
	"cast_site": func() Action { return &CastSite{} },
}

func (e *ActionAlias) UnmarshalJSON(data []byte) error {
	*e = make(map[string]Action)
	var tmp map[string]json.RawMessage
	err := json.Unmarshal(data, &tmp)
	if err != nil {
		log.Print(err)
		return err
	}
	for k, v := range tmp {
		var obj map[string]interface{}
		err := json.Unmarshal(v, &obj)
		if err != nil {
			log.Print(err)
			return err
		}
		if val, ok := obj["Type"]; ok {
			if f, ok := actionMap[val.(string)]; ok {
				action := f()
				err := json.Unmarshal(v, action)
				if err != nil {
					log.Println(err)
					return err
				}
				(*e)[k] = action
			}
		}
	}

	return nil
}
