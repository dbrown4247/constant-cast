package services

import (
	"fmt"
	"log"
	"os/exec"
)

func CastSite(device string, site string) (bool, error) {
	cmd := exec.Command("catt", "-d", device, "cast_site", site)
	_, err := cmd.Output()
	if err != nil {
		log.Println(err)
		return false, err
	}
	return true, nil
}

func CheckIdle(device string) (bool, error) {
	cmd := exec.Command("catt", "-d", device, "info")
	_, err := cmd.Output()
	if fmt.Sprint(err) == "exit status 1" {
		return true, nil
	}
	if err != nil {
		log.Println(err)
		return false, err
	}
	return false, nil
}
